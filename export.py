import pandas as pd

import constants


def get_class_index_name(index):
    if index is None:
        return 'None'
    return constants.system_classes[index].name


def tuple_to_str_names(mytup):
    ans = ""
    for x in mytup[:-1]:
        ans += get_class_index_name(x)
        ans += ", "
    ans += get_class_index_name(mytup[-1])
    return ans


def export_subpatterns(subpattern_instance_set: dict, filename):
    """
    :param subpattern_instance_set:
    example :
    { 'IAGG':[(12,23,14),(15,54,32),...,(34,21,54)],
    'CLI':[(2,3),(25,12),...,(77,82)],
    }
    :return: void
    """

    df = pd.DataFrame()
    for key in subpattern_instance_set.keys():
        df[key] = pd.Series(subpattern_instance_set[key]).apply(tuple_to_str_names)

    df.to_excel(filename)
    print(df)


def get_role_name_dict(pattern_name, pattern_instance: dict):
    if pattern_name == 'ADAPTER':
        index_tuple = pattern_instance.get('ICA')
        return {'Target': get_class_index_name(index_tuple[0]), 'Adapter': get_class_index_name(index_tuple[1]),
                'Adaptee': get_class_index_name(index_tuple[2])}

    elif pattern_name == 'PROXY':
        index_tuple = pattern_instance.get('ICA')
        if index_tuple:
            return {'Subject': get_class_index_name(index_tuple[0]), 'Proxy': get_class_index_name(index_tuple[1]),
                    'RealSubject': get_class_index_name(index_tuple[2])}
        else:
            index_tuple = pattern_instance.get('IASS')
            answer = {'Subject': get_class_index_name(index_tuple[0]), 'Proxy': get_class_index_name(index_tuple[1])}
            for cls in pattern_instance.get('CI'):
                if cls not in index_tuple:
                    answer['RealSubject'] = get_class_index_name(cls)
                    break
            return answer

    elif pattern_name == 'COMPOSITE':
        index_tuple = pattern_instance.get('IIAGG')
        if index_tuple:
            index_tuple = pattern_instance.get('IIAGG')
            answer = {'Comp': get_class_index_name(index_tuple[0]), 'Composite': get_class_index_name(index_tuple[1]),
                      'Composite1': get_class_index_name(index_tuple[2])}
            for cls in pattern_instance.get('CI'):
                if cls not in index_tuple:
                    answer['ConcreteComp'] = get_class_index_name(cls)
                    break
        elif pattern_instance.get('IAGG'):
            index_tuple = pattern_instance.get('IAGG')
            answer = {'Comp': get_class_index_name(index_tuple[0]), 'Composite': get_class_index_name(index_tuple[1])}
            for cls in pattern_instance.get('CI'):
                if cls not in index_tuple:
                    answer['ConcreteComp'] = get_class_index_name(cls)
                    break
        else:
            return {'Comp': pattern_instance.get('SAGG')[0]}


    elif pattern_name == 'BRIDGE':
        index_tuple = pattern_instance.get('IPAG')
        answer = {'Abstraction': get_class_index_name(index_tuple[0]),
                  'RefinedAbstraction': get_class_index_name(index_tuple[1]),
                  'Implementor': get_class_index_name(index_tuple[2])}
        index_tuple = pattern_instance.get('CI')
        answer['ConcreteImplementorA'] = get_class_index_name(index_tuple[2])
        answer['ConcreteImplementorB'] = get_class_index_name(index_tuple[3])
        return answer

    elif pattern_name == 'DECORATOR':
        index_tuple = pattern_instance.get('IAGG')
        answer = {'Comp': get_class_index_name(index_tuple[0]), 'Decorator': get_class_index_name(index_tuple[1])}
        for cls in pattern_instance.get('CI'):
            if cls not in index_tuple:
                answer['ConcreteComp'] = get_class_index_name(cls)
                break

        answer['ConcreteDecorator'] = get_class_index_name(pattern_instance.get('MLI')[2])

        return answer


    elif pattern_name == 'FLYWEIGHT':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Flyweight': get_class_index_name(index_tuple[0]),
                  'ConcreteFlyweight': get_class_index_name(index_tuple[1]),
                  'FlyweightFactory': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('CI')
        if index_tuple2[1] not in index_tuple:
            answer['UnsharedConFlyweight'] = index_tuple2[1]
        elif index_tuple2[2] not in index_tuple:
            answer['UnsharedConFlyweight'] = index_tuple2[2]
        return answer
    # elif pattern_name == 'FACADE':
    #     index_tuple = pattern_instance.get('ICD')
    #     answer = {'Facade': get_class_index_name(index_tuple[0]),
    #               'ConcreteFacade': get_class_index_name(index_tuple[1]),
    #               'SubSystem3': get_class_index_name(index_tuple[1])}
    #     index_tuple2 = pattern_instance.get('CI')
    #     if index_tuple2[1] not in index_tuple:
    #         answer['UnsharedConFlyweight'] = index_tuple2[1]
    #     elif index_tuple2[2] not in index_tuple:
    #         answer['UnsharedConFlyweight'] = index_tuple2[2]

    elif pattern_name == 'ABSTRACT FACTORY':
        index_tuple = pattern_instance.get('ICD')
        answer = {'AbstractFactory': get_class_index_name(index_tuple[0]),
                  'ConcreteFactory': get_class_index_name(index_tuple[1]),
                  'ConcreteProductA': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('CI')
        answer['AbstractProduct'] = index_tuple2[0]
        if index_tuple2[1] not in index_tuple:
            answer['ConcreteProductB'] = index_tuple2[1]
        else:
            answer['ConcreteProductB'] = index_tuple2[2]
        return answer
    elif pattern_name == 'BUILDER':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Builder': get_class_index_name(index_tuple[0]),
                  'ConcreteBuilder': get_class_index_name(index_tuple[1]),
                  'Director': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('ICA')
        answer['Product'] = index_tuple2[2]
        return answer
    elif pattern_name == 'FACTORY METHOD':
        index_tuple = pattern_instance.get('ICD')
        answer = {'Creator': get_class_index_name(index_tuple[0]),
                  'ConcreteCreator': get_class_index_name(index_tuple[1]),
                  'ConcreteProduct': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('DCI')
        answer['Product'] = index_tuple2[0]
        return answer
    elif pattern_name == 'PROTOTYPE':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Prototype': get_class_index_name(index_tuple[0]),
                  'ConcretePrototypeA': get_class_index_name(index_tuple[1]),
                  'client': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('CI')
        if index_tuple2[1] not in index_tuple:
            answer['ConcretePrototypeB'] = index_tuple2[1]
        else:
            answer['ConcretePrototypeB'] = index_tuple2[2]
        return answer
    elif pattern_name == 'SINGLETON':
        index_tuple = pattern_instance.get('SASS')
        answer = {'Singleton': get_class_index_name(index_tuple[0])}
        return answer
    elif pattern_name == 'CHAIN OF RESPONSIBILITY':
        index_tuple = pattern_instance.get('CI')
        answer = {'Handler': get_class_index_name(index_tuple[0]),
                  'ConcreteHandlerA': get_class_index_name(index_tuple[1]),
                  'ConcreteHandlerB': get_class_index_name(index_tuple[2])}
        return answer
    elif pattern_name == 'COMMAND':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Command': get_class_index_name(index_tuple[0]),
                  'ConcreteCommand': get_class_index_name(index_tuple[1]),
                  'Invoker': get_class_index_name(index_tuple[2])}
        index_tuple1 = pattern_instance.get('ICA')
        answer = {'Reciever': get_class_index_name(index_tuple1[2])}
        return answer
    elif pattern_name == 'INTERPRETER':
        index_tuple = pattern_instance.get('IAGG')
        answer = {'AbstractExpression': get_class_index_name(index_tuple[0]),
                  'NonTerminalExpression': get_class_index_name(index_tuple[1])}
        index_tuple2 = pattern_instance.get('IPD')
        answer = {'Content': get_class_index_name(index_tuple2[2])}
        index_tuple3 = pattern_instance.get('CI')
        if index_tuple3[1] not in index_tuple:
            answer['TerminalExpression'] = index_tuple3[1]
        else:
            answer['TerminalExpression'] = index_tuple3[2]
        return answer
    elif pattern_name == 'ITERATOR':
        index_tuple = pattern_instance.get('ICD')
        answer = {'Aggregate': get_class_index_name(index_tuple[0]),
                  'ConcreteAggregate': get_class_index_name(index_tuple[1]),
                  'ConcreteIterator': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('DCI')
        answer = {'Iterator': get_class_index_name(index_tuple2[0])}
        return answer
    elif pattern_name == 'MEDIATOR':
        index_tuple = pattern_instance.get('IPAS')
        answer = {'Colleague': get_class_index_name(index_tuple[0]),
                  'ConcreteColleagueB': get_class_index_name(index_tuple[1]),
                  'Mediator': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('ICA')
        answer = {'ConcreteMediator': get_class_index_name(index_tuple2[1])}
        answer = {'ConcreteColleagueA': get_class_index_name(index_tuple2[2])}
        return answer
    elif pattern_name == 'MEMENTO':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Memento': get_class_index_name(index_tuple[0]),
                  'MementoImp': get_class_index_name(index_tuple[1]),
                  'CareTaker': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('DPI')
        answer = {'Originator': get_class_index_name(index_tuple2[2])}
        return answer
    elif pattern_name == 'OBSERVER':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Observer': get_class_index_name(index_tuple[0]),
                  'ConcreteObserver': get_class_index_name(index_tuple[1]),
                  'Subject': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('ICD')
        answer = {'ConcreteSubject': get_class_index_name(index_tuple2[2])}
        return answer
    elif pattern_name == 'STATE':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Stateُ': get_class_index_name(index_tuple[0]),
                  'ConcreteStateA': get_class_index_name(index_tuple[1]),
                  'Context': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('CI')
        if index_tuple2[1] not in index_tuple:
            answer['ConcreteStateB'] = index_tuple2[1]
        elif index_tuple2[2] not in index_tuple:
            answer['ConcreteStateB'] = index_tuple2[2]
        return answer
    elif pattern_name == 'STRATEGY':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Stateُ': get_class_index_name(index_tuple[0]),
                  'ConcreteStrategyA': get_class_index_name(index_tuple[1]),
                  'Context': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('CI')
        if index_tuple2[1] not in index_tuple:
            answer['ConcreteStrategyB'] = index_tuple2[1]
        elif index_tuple2[2] not in index_tuple:
            answer['ConcreteStrategyB'] = index_tuple2[2]
        return answer
    elif pattern_name == 'TEMPLATE METHOD':
        index_tuple = pattern_instance.get('CI')
        answer = {'AbstractClass': get_class_index_name(index_tuple[0]),
                  'ConcreteClassA': get_class_index_name(index_tuple[1]),
                  'ConcreteClassB': get_class_index_name(index_tuple[2])}
        return answer
    elif pattern_name == 'VISITOR':
        index_tuple = pattern_instance.get('AGPI')
        answer = {'Element': get_class_index_name(index_tuple[0]),
                  'ConcreteElement': get_class_index_name(index_tuple[1]),
                  'ObjectStructure': get_class_index_name(index_tuple[2])}
        index_tuple2 = pattern_instance.get('ICD')
        answer = {'Visitor': get_class_index_name(index_tuple2[0])}
        answer = {'ConcreteVisitor': get_class_index_name(index_tuple2[1])}
        return answer


def partial_pattern_instance_dict_to_str(pattern_name, pattern_instance):
    """

    :param pattern_name: pattern's name
    :param pattern_instance:tuple
    list of dicts (key(SP name)-> value(tuple))
    :return:
    """
    new_dict = {}
    new_dict['info'] = get_role_name_dict(pattern_name, pattern_instance[1])
    new_dict['similarity'] = pattern_instance[0]

    import json
    return json.dumps(new_dict)


def pattern_instance_dict_to_str(pattern_name, pattern_instance: dict):
    """

    :param pattern_name: pattern's name
    :param pattern_instance:
    list of dicts (key(SP name)-> value(tuple))
    :return:
    """

    new_dict = get_role_name_dict(pattern_name, pattern_instance)
    import json
    return json.dumps(new_dict)


def export_patterns_partial(all_patterns_instance_set: dict, filename):
    """
    :param all_patterns_instance_set:

    dictionary: key(pattern_name) -> value (pattern_instance_set)
    pattern_instance_set: list of dictionaries (key(SP name)-> value(tuple))
    :return:
    """

    df = pd.DataFrame()
    for key in all_patterns_instance_set.keys():
        df[key] = pd.Series(all_patterns_instance_set[key]).apply(
            lambda x: partial_pattern_instance_dict_to_str(key, x))

    df.to_excel(filename)
    print(df)


def export_patterns(all_patterns_instance_set: dict, filename):
    """
    :param all_patterns_instance_set:

    dictionary: key(pattern_name) -> value (pattern_instance_set)
    pattern_instance_set: list of dictionaries (key(SP name)-> value(tuple))
    :return:
    """

    df = pd.DataFrame()
    for key in all_patterns_instance_set.keys():
        df[key] = pd.Series(all_patterns_instance_set[key]).apply(lambda x: pattern_instance_dict_to_str(key, x))

    df.to_excel(filename)
    print(df)
