import xml
from xml.dom.minidom import parse


import constants
from graph_matching import calculate_all_subpatterns, calculate_all_patterns, method_signatures
from parsers import getConnectionsList, getClassesList, create_matrix
from export import export_patterns, export_subpatterns

if __name__ == "__main__":
    doc = xml.dom.minidom.parse("JHotDraw-XMl1.1.xml")
    connection_names = ["Association", "Generalization", "Dependency"]
    connections = getConnectionsList(doc, connection_names)
    constants.system_classes = getClassesList(doc)

    system_matrix = create_matrix(classes=constants.system_classes, connections=connections)
    all_subpattern_instance_sets = calculate_all_subpatterns(constants.subpattern_matrices,
                                                                            system_matrix)
    #export_subpatterns(all_subpattern_instance_sets, 'JHotDraw-subpatterns2.xlsx')

    # dict name->list of tuples
    all_pattern_instance_sets_raw = calculate_all_patterns(constants.patterns,
                                                                          all_subpattern_instance_sets)

    #export_patterns(all_pattern_instance_sets_raw, 'JHotDraw-patterns.xlsx')
    # dict name -> list of dicts
    after_method_checking = method_signatures(all_pattern_instance_sets_raw, constants.system_classes)
    # dict name -> list of dicts

    export_patterns(after_method_checking, 'JHotDraw-Method.xlsx')