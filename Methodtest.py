import parsers

op = parsers.Operation("test", class_name="test", return_type="void", visibility="protected")
ops = []
ops.append(op)
class1 = parsers.Class("test", 1, operations=ops, visibility="protected")
class2 = parsers.Class("ffff", 544, "fsdfdsf", visibility="protected")
parentClass = parsers.Class("cloneable", 66, ops)
class3 = parsers.Class("ProtoTest", 6666, ops, parent="cloneable")
classes = []
classes.append(class1)
classes.append(class2)
classes.append(class3)

patterns_dict = {"SINGLETON": [{"SASS": (0)}],
                 "PROTOTYPE": [{"CI": (0, 1, 2)}]
                 }


def method_signatures(patterns_dict: dict, classes: list):
    final_patterns = {}
    for x in patterns_dict.keys():
        # if x == "ADAPTER":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICA')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         for q in j.invokes:
        #                             if q.name == k.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "PROXY":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         if 'ICA' in l.keys():
        #             b = False
        #             for i in method1:
        #                 for j in method2:
        #                     if i.name == j.name:
        #                         for k in method3:
        #                             for q in j.invokes:
        #                                 if q.name == k.name:
        #                                     lists.append(l)
        #                                     b = True
        #                                     break
        #                 if b == False:
        #                     for j in method3:
        #                         if i.name == j.name:
        #                             for k in method2:
        #                                 for q in j.invokes:
        #                                     if q.name == k.name:
        #                                         lists.append(l)
        #
        #         else:
        #             sp_classes1 = l.get('CI')
        #             method11 = classes[sp_classes1[0]].operations
        #             method21 = classes[sp_classes1[1]].operations
        #             b = False
        #             for i in method11:
        #                 for j in method21:
        #                     if i.name == j.name:
        #                         for k in method3:
        #                             for q in j.invokes:
        #                                 if q.name == k.name:
        #                                     lists.append(l)
        #                                     b = True
        #                                     break
        #                 if b == False:
        #                     for j in method21:
        #                         if i.name == j.name:
        #                             for k in method2:
        #                                 for q in j.invokes:
        #                                     if q.name == k.name:
        #                                         lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "COMPOSITE":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         if 'SAGG' in l.keys():
        #             lists.append(l)
        #         elif 'IAGG' in l.keys():
        #             sp_classes = l.get('IAGG')
        #             method1 = classes[sp_classes[0]].operations
        #             method2 = classes[sp_classes[1]].operations
        #             for i in method1:
        #                 for j in method2:
        #                     if i.name == j.name:
        #                         for q in j.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #
        #         elif 'IIAGG' in l.keys():
        #             sp_classes = l.get('IIAGG')
        #             method1 = classes[sp_classes[0]].operations
        #             method2 = classes[sp_classes[1]].operations
        #             for i in method1:
        #                 for j in method2:
        #                     if i.name == j.name:
        #                         for q in j.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "BRIDGE":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('IPAG')
        #         method11 = classes[sp_classes1[0]].operations
        #         method21 = classes[sp_classes1[1]].operations
        #         for i in method11:
        #             for j in method21:
        #                 if i.name == j.name:
        #                     b = False
        #                     for k in method1:
        #                         for m in method2:
        #                             if k.name == m.name:
        #                                 for q in j.invokes:
        #                                     if q.name == k.name:
        #                                         lists.append(l)
        #                                         b = True
        #                                         break
        #                     if b == False:
        #                         for k in method1:
        #                             for m in method3:
        #                                 if k.name == m.name:
        #                                     for q in j.invokes:
        #                                         if q.name == k.name:
        #                                             lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "DECORATOR":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('IAGG')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         sp_classes1 = l.get('CI')
        #         method11 = classes[sp_classes1[0]].operations
        #         method21 = classes[sp_classes1[1]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for q in j.invokes:
        #                         if q.name == i.name:
        #                             lists.append(l)
        #                 elif i.name == j.name:
        #                     b = False
        #                     for m in method21:
        #                         if i.name == m.name:
        #                             for q in m.invokes:
        #                                 if q.name == i.name:
        #                                     lists.append(l)
        #                                     b = True
        #                                     break
        #                     if b == False:
        #                         for m in method31:
        #                             if i.name == m.name:
        #                                 for q in m.invokes:
        #                                     if q.name == i.name:
        #                                         lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "FLYWEIGHT":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('AGPI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method3:
        #             if i.return_type == classes[sp_classes[0]].name:
        #                 for j in method2:
        #                     if j.name == classes[sp_classes[1]].name:
        #                         for q in i.invokes:
        #                             if q.name == j.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "FACADE":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICD')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         for q in j.invokes:
        #                             if q.name == k.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "ABSTRACT FACTORY":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICD')
        #         method1 = classes[sp_classes[0]].operations
        #         sp_classes1 = l.get('DCI')
        #         method31 = classes[sp_classes1[2]].operations
        #         sp_classes2 = l.get('CI')
        #         method22 = classes[sp_classes2[1]].operations
        #         method32 = classes[sp_classes2[2]].operations
        #         for i in method1:
        #             for j in method31:
        #                 if i == j:
        #                     b = True
        #                     if j.return_type == classes[sp_classes2[1]].name:
        #                         for m in method22:
        #                             if m == classes[sp_classes2[1]].name:
        #                                 for q in j.invokes:
        #                                     if q.name == m.name:
        #                                         lists.append(l)
        #                                         b = True
        #                                         break
        #                     if b == False:
        #                         if j.return_type == classes[sp_classes2[2]].name:
        #                             for m in method32:
        #                                 if m.name == classes[sp_classes2[2]].name:
        #                                     for q in j.invokes:
        #                                         if q.name == m.name:
        #                                             lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "BUILDER":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICA')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('AGPI')
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         for q in k.invokes:
        #                             if q.name == k.name:
        #                                 for m in method31:
        #                                     for z in m.invokes:
        #                                         if z.name == i.name or z.name == j.name:
        #                                             lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "FACTORY METHOD":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICD')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         sp_classes1 = l.get('DCI')
        #         method11 = classes[sp_classes1[0]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method11:
        #                         for q in j.invokes:
        #                             if q.name == k.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        if x == "PROTOTYPE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                if classes[sp_classes[1]].parent == "cloneable":
                    lists.append(l)
                elif classes[sp_classes[2]].parent == "cloneable":
                    lists.append(l)
            final_patterns[x] = lists
        elif x == "SINGLETON":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('SASS')
                methods = classes[sp_classes].operations
                for z in methods:
                    if z.name == classes[sp_classes].name:
                        if z.visibility == "protected":
                            lists.append(l)
                            break
                        elif z.visibility == "private":
                            lists.append(l)
                            break
                        elif z.visibility == "public":
                            if z.static == 1:
                                lists.append(l)
                                break
            final_patterns[x] = lists
        # elif x == "CHAIN OF RESPONSIBILITY":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 for k in method3:
        #                     if i.name == j.name and i.name == k.name:
        #                         b = False
        #                         for q in j.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #                                 b = True
        #                         if b == False:
        #                             for q in k.invokes:
        #                                 if q.name == i.name:
        #                                     lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "COMMAND":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICA')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('AGPI')
        #         method11 = classes[sp_classes1[0]].operations
        #         method21 = classes[sp_classes1[1]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         b = False
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         for q in j.invokes:
        #                             if q.name == k.name:
        #                                 b = True
        #                                 break
        #         if b == True:
        #             for i in method31:
        #                 for j in method11:
        #                     for q in j.invokes:
        #                         if q.name == i.name:
        #                             for m in method21:
        #                                 if j.name == m.name:
        #                                     lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "INTERPRETER":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         b = False
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('IPD')
        #         method21 = classes[sp_classes1[1]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 for k in method3:
        #                     if i.name == j.name and i.name == k.name:
        #                         b = True
        #                         break
        #         if b == True:
        #             c = False
        #             for j in method2:
        #                 for i in method21:
        #                     if j.name != i.name:
        #                         for m in method31:
        #                             for q in j.invokes:
        #                                 if q.name == m.name:
        #                                     lists.append(l)
        #                                     c = True
        #                                     break
        #             if c == False:
        #                 for j in method3:
        #                     for i in method21:
        #                         if j.name != i.name:
        #                             for m in method31:
        #                                 for q in j.invokes:
        #                                     if q.name == m.name:
        #                                         lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "ITERATOR":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICD')
        #         if sp_classes[2].return_type == classes[sp_classes[1]].name:
        #             lists.append(l)
        #             break
        #     final_patterns[x] = lists
        # elif x == "MEDIATOR":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         b = False
        #         sp_classes = l.get('IPAS')
        #         method1 = classes[sp_classes[0]].operations
        #         sp_classes1 = l.get('ICA')
        #         method21 = classes[sp_classes1[1]].operations
        #         method11 = classes[sp_classes1[0]].operations
        #         for i in method11:
        #             for j in method21:
        #                 if i.name == j.name:
        #                     b = True
        #                     break
        #         if b == True:
        #             for i in method1:
        #                 if i.return_type == classes[sp_classes[2]].name:
        #                     for j in method21:
        #                         for q in j.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "MEMENTO":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         b = False
        #         sp_classes = l.get('AGPI')
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('DPI')
        #         method21 = classes[sp_classes1[1]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method3:
        #             if i.return_type == classes[sp_classes[0]].name:
        #                 b = True
        #                 break
        #         if b == True:
        #             for i in method21:
        #                 for j in method31:
        #                     for q in j.invokes:
        #                         if q.name == i.name:
        #                             lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "OBSERVER":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('AGPI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for m in method3:
        #                         for q in m.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "STATE":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             b = False
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         if k.name == classes[sp_classes[1]].name:
        #                             for q in j.invokes:
        #                                 if q.name == k.name:
        #                                     lists.append(l)
        #                                     b = True
        #                                     break
        #             if b == False:
        #                 for j in method3:
        #                     if i.name == j.name:
        #                         for k in method2:
        #                             if k.name == classes[sp_classes[2]].name:
        #                                 for q in j.invokes:
        #                                     if q.name == k.name:
        #                                         lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "STRATEGY":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('AGPI')
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method1:
        #             b = False
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for m in method31:
        #                         for q in m.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #                                 b = True
        #                                 break
        #             if b == False:
        #                 for j in method3:
        #                     if i.name == j.name:
        #                         for m in method31:
        #                             for q in m.invokes:
        #                                 if q.name == i.name:
        #                                     lists.append(l)
        #                                     b = True
        #     final_patterns[x] = lists
        # elif x == "TEMPLATE METHOD":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for q in i.invokes:
        #                 if q.name == i.name:
        #                     b = False
        #                     for j in method2:
        #                         if i.name == j.name:
        #                             lists.append(l)
        #                             b = True
        #                             break
        #                     if b == False:
        #                         for j in method3:
        #                             if i.name == j.name:
        #                                 lists.append(l)
        #                                 break
        #     final_patterns[x] = lists
        # elif x == "VISITOR":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('AGPI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         sp_classes1 = l.get('ICD')
        #         method11 = classes[sp_classes1[0]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         b = False
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     b = True
        #                     break
        #         if b == True:
        #             for i in method31:
        #                 for j in method11:
        #                     for q in i.invokes:
        #                         if q.name == j.name:
        #                             lists.append(l)
        #     final_patterns[x] = lists
    return final_patterns


def main():
    test = method_signatures(patterns_dict, classes)
    print(test)


if __name__ == "__main__":
    main()
