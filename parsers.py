import xml.dom.minidom
import numpy as np


class Class:
    def __init__(self, name, id, operations, parent=None, visibility=None):
        self.name = name
        self.id = id
        self.operations = operations
        self.parent = parent
        self.visibility = visibility


class Operation:
    def __init__(self, name, class_name, return_type, static=0, visibility=None):
        self.name = name
        self.return_type = return_type
        self.class_name = class_name
        self.static = static
        self.visibility = visibility

    def toString(self):
        print(
            self.name + " -- " + "class_name : " + self.class_name + " -- " + "return_type : " + self.return_type + " -- " + "static : " + str(
                self.static) + " -- " + "visibility : " + self.visibility)


class Conection:
    def __init__(self, conection_name, source, target, id, visibility=None):
        self.conection_name = conection_name
        self.source = source
        self.target = target
        self.id = id
        self.visibility = visibility

    def toString(self):
        print(self.conection_name + " -- " + self.source + " --> " + self.target)


def getClassesList(doc):
    classes = []
    expertise = doc.getElementsByTagName("UML:Class")
    expertise2 = doc.getElementsByTagName("UML:Interface")
    all_expertise = expertise + expertise2
    for eachClass in all_expertise:
        name = eachClass.getAttribute("name")
        id = eachClass.getAttribute("id")
        operations = getOperationsList(doc, name)
        tags = eachClass.getElementsByTagName("UML:TaggedValue")
        parent = ""
        for tag in tags:
            if tag.getAttribute("tag") == "genlinks":
                parent = tag.getAttribute("value")
                p = parent.split("=")
                parent = p[1].replace(";", "")

        c = Class(name, id, operations, parent)
        classes.append(c)
    return classes


def getOperationsList(doc, class_name):
    expertise = doc.getElementsByTagName("UML:Class")
    all_operations = []
    for each in expertise:
        if each.getAttribute("name") == class_name:
            classifier = each.getElementsByTagName("UML:Classifier.feature")
            if classifier:
                operations = classifier[0].getElementsByTagName("UML:Operation")
                for op in operations:
                    name = op.getAttribute("name")
                    visibility = op.getAttribute("visibility")
                    tags = op.getElementsByTagName("UML:TaggedValue")
                    ststic = 0
                    return_type = ""
                    for t in tags:
                        if t.getAttribute("tag") == "ststic":
                            print("hiiii")
                            ststic = t.getAttribute("value")
                        if t.getAttribute("tag") == "type":
                            return_type = t.getAttribute("value")
                    oper = Operation(name, class_name, return_type, ststic, visibility)
                    all_operations.append(oper)
    return all_operations


def getConnectionsList(doc, connection_names):
    connections = []
    for connection_name in connection_names:
        expertise = doc.getElementsByTagName("UML:" + connection_name)
        for skill in expertise:
            id = skill.getAttribute("xmi.id")
            tags = skill.getElementsByTagName("UML:TaggedValue")
            source = ""
            target = ""
            connectionName = ""
            for tag in tags:
                if tag.getAttribute("tag") == "ea_sourceName":
                    source = tag.getAttribute("value")
                if tag.getAttribute("tag") == "ea_targetName":
                    target = tag.getAttribute("value")
                if tag.getAttribute("tag") == "ea_type":
                    connectionName = tag.getAttribute("value")
            c = Conection(connectionName, source, target, id)
            connections.append(c)
    return connections


def create_matrix(connections, classes):
    weights = {"Association": 2, "Inheritance": 3, "Aggregation": 5, "Dependency": 7, "Generalization": 3,
               "Realisation": 3}
    classes = np.array(classes)
    connections = np.array(connections)
    dim = len(classes)
    matrix = np.ones((dim, dim), dtype=int)

    for l, i in enumerate(connections):
        # print("connections"+str(l)+"\n")
        # print("source : "+i.source)
        # print("target : "+i.target)
        # print("-----------")
        conection_name = i.conection_name
        for h in weights.keys():
            if h == conection_name:
                conection_weight = weights.get(h)
                # print(conection_weight)
        for c, j in enumerate(classes):
            if j.name == i.source:
                sourceIndex = c
            if j.name == i.target:
                targetIndex = c
        # print(sourceIndex)
        # print(targetIndex)

        if matrix[sourceIndex, targetIndex] != 0 and (matrix[sourceIndex, targetIndex] % conection_weight != 0):
            new_conection_weight = conection_weight * matrix[sourceIndex, targetIndex]
            matrix[sourceIndex, targetIndex] = new_conection_weight
        elif matrix[sourceIndex, targetIndex] == 0:
            matrix[sourceIndex, targetIndex] = conection_weight
    return matrix


def main():
    doc = xml.dom.minidom.parse("JUnit-XMl1.1.xml")
    connection_names = ["Association", "Generalization", "Dependency"]
    connections = getConnectionsList(doc, connection_names)
    # print(connections)
    # 59 ,11=9
    # 84,17 = 27
    # 84 ,13 =4
    classes = getClassesList(doc)
    # print(classes[84].name)
    # print(classes[17].name)
    matrix = create_matrix(connections, classes)
    # print(matrix[84,13])
    for i in matrix:
        print(i)
        print("----------")


if __name__ == "__main__":
    main()
