import numpy as np
from itertools import product

"""
association = 2
inheritance = 3 ( r(ci,cj) , M[j,i])
aggregation = 5
dependency = 7
"""

"""subpatterns matrices: """

"""Table 1(sub-pattern recognition)"""


def has_contrary_edge(sequence: tuple, subpattern_matrix: np.ndarray, system_matrix: np.ndarray):
    for i, x in enumerate(sequence):
        for j, y in enumerate(sequence):
            if (system_matrix[x][y] % subpattern_matrix[i][j]) != 0:
                return True
    return False


def detect_subpatterns(subpattern_matrix: np.ndarray, system_matrix: np.ndarray):
    identifiedSubpatternSet = []

    k = len(subpattern_matrix)
    candidate_vertex_set = [[]] * k  # inner list contains node numbers of system matrix
    sys_cw_outs = np.prod(system_matrix, axis=1)
    sys_cw_ins = np.prod(system_matrix, axis=0)
    subpattern_cw_outs = np.prod(subpattern_matrix, axis=1)
    subpattern_cw_ins = np.prod(subpattern_matrix, axis=0)
    for i in range(k):  # First Part
        for j in range(len(system_matrix)):
            if sys_cw_ins[j] % subpattern_cw_ins[i] == 0 and \
                    sys_cw_outs[j] % subpattern_cw_outs[i] == 0:
                if j not in candidate_vertex_set[i]:
                    candidate_vertex_set[i].append(j)
    # First Part done

    # Second Part
    import itertools
    permutation_list = list(itertools.product(*candidate_vertex_set))
    for sequence in permutation_list:
        if has_contrary_edge(sequence, subpattern_matrix, system_matrix):
            continue
        else:
            if sequence not in identifiedSubpatternSet:
                identifiedSubpatternSet.append(sequence)
    # Second Part done
    return identifiedSubpatternSet  # list of tuples


"""TABLE 1(sub-pattern recognition) DONE"""

"""calculate All sub-patterns"""


# subpattern matrices is a list of tuples
def calculate_all_subpatterns(subpattern_matrices: list, system_matrix: np.ndarray):
    subpattern_instance_set = {}
    for subpattern_matrix in subpattern_matrices:
        subpattern_instance_set[subpattern_matrix[0]] = detect_subpatterns(subpattern_matrix[1], system_matrix)
    return subpattern_instance_set


"""TABLE 2 : Algorithm of combining sub-patterns."""


def partial_check_mode_for_combination(combination, mode_info, subpattern_instances):
    total_count = 0
    similar_count = 0
    GCDR = {}
    subpatterns = mode_info[0]
    if len(subpatterns) > 1 and subpatterns[1].startswith('!'):
        return None
    for i, sp in enumerate(subpatterns):
        GCDR[sp] = []
        for j in range(len(subpattern_instances[i][0])):
            GCDR[sp].append(None)

    intersection_matrix = mode_info[1]
    if len(intersection_matrix) == 1:
        GCDR[subpatterns[0]] = (subpattern_instances[0][combination[0]])
        return (100, GCDR)
    else:
        for i in range(len(intersection_matrix)):
            for j in range(i + 1, len(intersection_matrix)):
                mapping_dict = intersection_matrix[i][j]
                total_count += len(mapping_dict)
                for k in mapping_dict.keys():
                    # below if just used for Adapter
                    if subpatterns[i].startswith('!') or subpatterns[j].startswith('!'):
                        if subpattern_instances[0][combination[0]][k - 1] != subpattern_instances[1][combination[1]][
                            mapping_dict[k] - 1]:
                            return True  # one uneqaul memeber is enough e.g. for Adapter

                    elif (subpattern_instances[i])[combination[i]][k - 1] != (subpattern_instances[j])[combination[j]][
                        mapping_dict[k] - 1]:
                        pass
                    else:
                        similar_count += 1
                        GCDR[subpatterns[i]][k - 1] = subpattern_instances[i][combination[i]][k - 1]
                        GCDR[subpatterns[j]][mapping_dict[k] - 1] = subpattern_instances[j][combination[j]][
                            mapping_dict[k] - 1]

                # combination = list of indexes for each subpattern_instance in its identifiedSubpatternSet in order
                # of subpatterns in mode info

    for sp in subpatterns:
        GCDR[sp] = tuple(GCDR[sp])
    return ((similar_count * 100.0 / total_count), GCDR)


def check_mode_for_combination(combination, mode_info, subpattern_instances):
    subpatterns = mode_info[0]
    intersection_matrix = mode_info[1]
    if len(intersection_matrix) == 1:
        return True
    else:
        for i in range(len(intersection_matrix)):
            for j in range(i + 1, len(intersection_matrix)):
                mapping_dict = intersection_matrix[i][j]
                for k in mapping_dict.keys():
                    # below if just used for Adapter
                    if subpatterns[i].startswith('!') or subpatterns[j].startswith('!'):
                        if subpattern_instances[0][combination[0]][k - 1] != subpattern_instances[1][combination[1]][
                            mapping_dict[k] - 1]:
                            return True  # one uneqaul memeber is enough e.g. for Adapter

                    elif (subpattern_instances[i])[combination[i]][k - 1] != (subpattern_instances[j])[combination[j]][
                        mapping_dict[k] - 1]:
                        return False
                    # combination = list of indexes for each subpattern_instance in its identifiedSubpatternSet in order of subpatterns in mode info
    return True


def detect_pattern_partial(pattern_sfm_modes: list, all_subpatterns_instance_set: dict):
    pattern_instance_set = []
    for mode_info in pattern_sfm_modes:
        subpatterns = mode_info[0]
        subpatterns_instances = [all_subpatterns_instance_set.get(x.replace('!', '')) for x in subpatterns]
        # list of list of tuples [[(31,24,56),(23,2,4),...],[(21,4,16),(23,2,4),...]]

        indexes_list = [list(range(len(x))) for x in
                        subpatterns_instances]  # list of list of ranges.e.g. [[0,1,2],[0,1],[0,1,2,3,4,5]]
        combinations = list(product(*indexes_list))
        # list of tuples [...(0,1,1),(0,1,2),...] length of tuple shows number of SPs
        counter = 0
        for combination in combinations:
            # if check_mode_for_combination(combination, mode_info, subpatterns_instances):
            partial_GCDR_tuple = partial_check_mode_for_combination(combination, mode_info, subpatterns_instances)

            if not partial_GCDR_tuple in pattern_instance_set and partial_GCDR_tuple and partial_GCDR_tuple[0] > 30:
                pattern_instance_set.append(partial_GCDR_tuple)

        counter += 1
    return pattern_instance_set  # list of dictionaries with k:v like SPname->tuple of nodes


# SFM of format (CDI,SAGG) / (CID,!IAGG)

def detect_pattern(pattern_sfm_modes: list, all_subpatterns_instance_set: dict):
    pattern_instance_set = []
    for mode_info in pattern_sfm_modes:
        subpatterns = mode_info[0]
        subpatterns_instances = [all_subpatterns_instance_set.get(x.replace('!', '')) for x in subpatterns]
        # list of list of tuples [[(31,24,56),(23,2,4),...],[(21,4,16),(23,2,4),...]]

        indexes_list = [list(range(len(x))) for x in
                        subpatterns_instances]  # list of list of ranges.e.g. [[0,1,2],[0,1],[0,1,2,3,4,5]]
        combinations = list(product(*indexes_list))
        # list of tuples [...(0,1,1),(0,1,2),...] length of tuple shows number of SPs
        counter = 0
        for combination in combinations:
            if check_mode_for_combination(combination, mode_info, subpatterns_instances):
                GCDR_combined = {}
                for i, sp in enumerate(subpatterns):
                    if not sp.startswith('!'):
                        GCDR_combined[sp] = subpatterns_instances[i][combination[i]]
                if not GCDR_combined in pattern_instance_set:
                    pattern_instance_set.append(GCDR_combined)
            else:
                if subpatterns[1].startswith('!'):  # for Adapter
                    GCDR_combined = {subpatterns[0]: subpatterns_instances[0][combination[0]]}
                    try:
                        pattern_instance_set.remove(GCDR_combined)
                        print('did it')
                    except:
                        pass
                    for comb in combinations:
                        if comb[0] == combination[0]:
                            combinations.remove(comb)
            counter += 1
    return pattern_instance_set  # list of dictionaries with k:v like SPname->tuple of nodes


def calculate_all_patterns_partial(pattern_sfms: dict, all_subpatterns_instance_set):
    all_patterns_instances_set = {}
    for pattern_name in pattern_sfms.keys():
        all_patterns_instances_set[pattern_name] = detect_pattern_partial(pattern_sfms[pattern_name],
                                                                          all_subpatterns_instance_set)
    return all_patterns_instances_set
    # key : pattern name -> value :  list of dictionaries with k:v like SPname->tuple of nodes


def calculate_all_patterns(pattern_sfms: dict, all_subpatterns_instance_set):
    all_patterns_instances_set = {}
    for pattern_name in pattern_sfms.keys():
        all_patterns_instances_set[pattern_name] = detect_pattern(pattern_sfms[pattern_name],
                                                                  all_subpatterns_instance_set)
    return all_patterns_instances_set
    # key : pattern name -> value :  list of dictionaries with k:v like SPname->tuple of nodes


def method_signatures(patterns_dict: dict, classes: list):
    final_patterns = {}
    for x in patterns_dict.keys():
        # if x == "ADAPTER":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICA')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         for q in j.invokes:
        #                             if q.name == k.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "PROXY":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         if 'ICA' in l.keys():
        #             b = False
        #             for i in method1:
        #                 for j in method2:
        #                     if i.name == j.name:
        #                         for k in method3:
        #                             for q in j.invokes:
        #                                 if q.name == k.name:
        #                                     lists.append(l)
        #                                     b = True
        #                                     break
        #                 if b == False:
        #                     for j in method3:
        #                         if i.name == j.name:
        #                             for k in method2:
        #                                 for q in j.invokes:
        #                                     if q.name == k.name:
        #                                         lists.append(l)
        #
        #         else:
        #             sp_classes1 = l.get('CI')
        #             method11 = classes[sp_classes1[0]].operations
        #             method21 = classes[sp_classes1[1]].operations
        #             b = False
        #             for i in method11:
        #                 for j in method21:
        #                     if i.name == j.name:
        #                         for k in method3:
        #                             for q in j.invokes:
        #                                 if q.name == k.name:
        #                                     lists.append(l)
        #                                     b = True
        #                                     break
        #                 if b == False:
        #                     for j in method21:
        #                         if i.name == j.name:
        #                             for k in method2:
        #                                 for q in j.invokes:
        #                                     if q.name == k.name:
        #                                         lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "COMPOSITE":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         if 'SAGG' in l.keys():
        #             lists.append(l)
        #         elif 'IAGG' in l.keys():
        #             sp_classes = l.get('IAGG')
        #             method1 = classes[sp_classes[0]].operations
        #             method2 = classes[sp_classes[1]].operations
        #             for i in method1:
        #                 for j in method2:
        #                     if i.name == j.name:
        #                         for q in j.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #
        #         elif 'IIAGG' in l.keys():
        #             sp_classes = l.get('IIAGG')
        #             method1 = classes[sp_classes[0]].operations
        #             method2 = classes[sp_classes[1]].operations
        #             for i in method1:
        #                 for j in method2:
        #                     if i.name == j.name:
        #                         for q in j.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "BRIDGE":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('IPAG')
        #         method11 = classes[sp_classes1[0]].operations
        #         method21 = classes[sp_classes1[1]].operations
        #         for i in method11:
        #             for j in method21:
        #                 if i.name == j.name:
        #                     b = False
        #                     for k in method1:
        #                         for m in method2:
        #                             if k.name == m.name:
        #                                 for q in j.invokes:
        #                                     if q.name == k.name:
        #                                         lists.append(l)
        #                                         b = True
        #                                         break
        #                     if b == False:
        #                         for k in method1:
        #                             for m in method3:
        #                                 if k.name == m.name:
        #                                     for q in j.invokes:
        #                                         if q.name == k.name:
        #                                             lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "DECORATOR":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('IAGG')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         sp_classes1 = l.get('CI')
        #         method11 = classes[sp_classes1[0]].operations
        #         method21 = classes[sp_classes1[1]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for q in j.invokes:
        #                         if q.name == i.name:
        #                             lists.append(l)
        #                 elif i.name == j.name:
        #                     b = False
        #                     for m in method21:
        #                         if i.name == m.name:
        #                             for q in m.invokes:
        #                                 if q.name == i.name:
        #                                     lists.append(l)
        #                                     b = True
        #                                     break
        #                     if b == False:
        #                         for m in method31:
        #                             if i.name == m.name:
        #                                 for q in m.invokes:
        #                                     if q.name == i.name:
        #                                         lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "FLYWEIGHT":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('AGPI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method3:
        #             if i.return_type == classes[sp_classes[0]].name:
        #                 for j in method2:
        #                     if j.name == classes[sp_classes[1]].name:
        #                         for q in i.invokes:
        #                             if q.name == j.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "FACADE":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICD')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         for q in j.invokes:
        #                             if q.name == k.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        # elif x == "ABSTRACT FACTORY":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICD')
        #         method1 = classes[sp_classes[0]].operations
        #         sp_classes1 = l.get('DCI')
        #         method31 = classes[sp_classes1[2]].operations
        #         sp_classes2 = l.get('CI')
        #         method22 = classes[sp_classes2[1]].operations
        #         method32 = classes[sp_classes2[2]].operations
        #         for i in method1:
        #             for j in method31:
        #                 if i == j:
        #                     b = True
        #                     if j.return_type == classes[sp_classes2[1]].name:
        #                         for m in method22:
        #                             if m == classes[sp_classes2[1]].name:
        #                                 for q in j.invokes:
        #                                     if q.name == m.name:
        #                                         lists.append(l)
        #                                         b = True
        #                                         break
        #                     if b == False:
        #                         if j.return_type == classes[sp_classes2[2]].name:
        #                             for m in method32:
        #                                 if m.name == classes[sp_classes2[2]].name:
        #                                     for q in j.invokes:
        #                                         if q.name == m.name:
        #                                             lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "BUILDER":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICA')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('AGPI')
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         for q in k.invokes:
        #                             if q.name == k.name:
        #                                 for m in method31:
        #                                     for z in m.invokes:
        #                                         if z.name == i.name or z.name == j.name:
        #                                             lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "FACTORY METHOD":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICD')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         sp_classes1 = l.get('DCI')
        #         method11 = classes[sp_classes1[0]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method11:
        #                         for q in j.invokes:
        #                             if q.name == k.name:
        #                                 lists.append(l)
        #
        #     final_patterns[x] = lists
        if x == "PROTOTYPE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                if classes[sp_classes[1]].parent == "cloneable":
                    lists.append(l)
                elif classes[sp_classes[2]].parent == "cloneable":
                    lists.append(l)
            final_patterns[x] = lists
        elif x == "SINGLETON":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('SASS')
                methods = classes[sp_classes].operations
                for z in methods:
                    if z.name == classes[sp_classes].name:
                        if z.visibility == "protected":
                            lists.append(l)
                            break
                        elif z.visibility == "private":
                            lists.append(l)
                            break
                        elif z.visibility == "public":
                            if z.static == 1:
                                lists.append(l)
                                break
            final_patterns[x] = lists
        # elif x == "CHAIN OF RESPONSIBILITY":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 for k in method3:
        #                     if i.name == j.name and i.name == k.name:
        #                         b = False
        #                         for q in j.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #                                 b = True
        #                         if b == False:
        #                             for q in k.invokes:
        #                                 if q.name == i.name:
        #                                     lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "COMMAND":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICA')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('AGPI')
        #         method11 = classes[sp_classes1[0]].operations
        #         method21 = classes[sp_classes1[1]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         b = False
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         for q in j.invokes:
        #                             if q.name == k.name:
        #                                 b = True
        #                                 break
        #         if b == True:
        #             for i in method31:
        #                 for j in method11:
        #                     for q in j.invokes:
        #                         if q.name == i.name:
        #                             for m in method21:
        #                                 if j.name == m.name:
        #                                     lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "INTERPRETER":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         b = False
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('IPD')
        #         method21 = classes[sp_classes1[1]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 for k in method3:
        #                     if i.name == j.name and i.name == k.name:
        #                         b = True
        #                         break
        #         if b == True:
        #             c = False
        #             for j in method2:
        #                 for i in method21:
        #                     if j.name != i.name:
        #                         for m in method31:
        #                             for q in j.invokes:
        #                                 if q.name == m.name:
        #                                     lists.append(l)
        #                                     c = True
        #                                     break
        #             if c == False:
        #                 for j in method3:
        #                     for i in method21:
        #                         if j.name != i.name:
        #                             for m in method31:
        #                                 for q in j.invokes:
        #                                     if q.name == m.name:
        #                                         lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "ITERATOR":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('ICD')
        #         if sp_classes[2].return_type == classes[sp_classes[1]].name:
        #             lists.append(l)
        #             break
        #     final_patterns[x] = lists
        # elif x == "MEDIATOR":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         b = False
        #         sp_classes = l.get('IPAS')
        #         method1 = classes[sp_classes[0]].operations
        #         sp_classes1 = l.get('ICA')
        #         method21 = classes[sp_classes1[1]].operations
        #         method11 = classes[sp_classes1[0]].operations
        #         for i in method11:
        #             for j in method21:
        #                 if i.name == j.name:
        #                     b = True
        #                     break
        #         if b == True:
        #             for i in method1:
        #                 if i.return_type == classes[sp_classes[2]].name:
        #                     for j in method21:
        #                         for q in j.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "MEMENTO":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         b = False
        #         sp_classes = l.get('AGPI')
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('DPI')
        #         method21 = classes[sp_classes1[1]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method3:
        #             if i.return_type == classes[sp_classes[0]].name:
        #                 b = True
        #                 break
        #         if b == True:
        #             for i in method21:
        #                 for j in method31:
        #                     for q in j.invokes:
        #                         if q.name == i.name:
        #                             lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "OBSERVER":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('AGPI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for m in method3:
        #                         for q in m.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "STATE":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             b = False
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for k in method3:
        #                         if k.name == classes[sp_classes[1]].name:
        #                             for q in j.invokes:
        #                                 if q.name == k.name:
        #                                     lists.append(l)
        #                                     b = True
        #                                     break
        #             if b == False:
        #                 for j in method3:
        #                     if i.name == j.name:
        #                         for k in method2:
        #                             if k.name == classes[sp_classes[2]].name:
        #                                 for q in j.invokes:
        #                                     if q.name == k.name:
        #                                         lists.append(l)
        #     final_patterns[x] = lists
        # elif x == "STRATEGY":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         sp_classes1 = l.get('AGPI')
        #         method31 = classes[sp_classes1[2]].operations
        #         for i in method1:
        #             b = False
        #             for j in method2:
        #                 if i.name == j.name:
        #                     for m in method31:
        #                         for q in m.invokes:
        #                             if q.name == i.name:
        #                                 lists.append(l)
        #                                 b = True
        #                                 break
        #             if b == False:
        #                 for j in method3:
        #                     if i.name == j.name:
        #                         for m in method31:
        #                             for q in m.invokes:
        #                                 if q.name == i.name:
        #                                     lists.append(l)
        #                                     b = True
        #     final_patterns[x] = lists
        # elif x == "TEMPLATE METHOD":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('CI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         method3 = classes[sp_classes[2]].operations
        #         for i in method1:
        #             for q in i.invokes:
        #                 if q.name == i.name:
        #                     b = False
        #                     for j in method2:
        #                         if i.name == j.name:
        #                             lists.append(l)
        #                             b = True
        #                             break
        #                     if b == False:
        #                         for j in method3:
        #                             if i.name == j.name:
        #                                 lists.append(l)
        #                                 break
        #     final_patterns[x] = lists
        # elif x == "VISITOR":
        #     lists = []
        #     class_list = patterns_dict.get(x)
        #     for l in class_list:
        #         sp_classes = l.get('AGPI')
        #         method1 = classes[sp_classes[0]].operations
        #         method2 = classes[sp_classes[1]].operations
        #         sp_classes1 = l.get('ICD')
        #         method11 = classes[sp_classes1[0]].operations
        #         method31 = classes[sp_classes1[2]].operations
        #         b = False
        #         for i in method1:
        #             for j in method2:
        #                 if i.name == j.name:
        #                     b = True
        #                     break
        #         if b == True:
        #             for i in method31:
        #                 for j in method11:
        #                     for q in i.invokes:
        #                         if q.name == j.name:
        #                             lists.append(l)
        #     final_patterns[x] = lists
    return final_patterns

def method_signatures_full(patterns_dict: dict, classes: list):
    final_patterns = {}
    for x in patterns_dict.keys():
        if x == "ADAPTER":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICA')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                for q in j.invokes:
                                    if q.name == k.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "PROXY":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                if 'ICA' in l.keys():
                    b = False
                    for i in method1:
                        for j in method2:
                            if i.name == j.name:
                                for k in method3:
                                    for q in j.invokes:
                                        if q.name == k.name:
                                            lists.append(l)
                                            b = True
                                            break
                        if b == False:
                            for j in method3:
                                if i.name == j.name:
                                    for k in method2:
                                        for q in j.invokes:
                                            if q.name == k.name:
                                                lists.append(l)

                else:
                    sp_classes1 = l.get('CI')
                    method11 = classes[sp_classes1[0]].operations
                    method21 = classes[sp_classes1[1]].operations
                    b = False
                    for i in method11:
                        for j in method21:
                            if i.name == j.name:
                                for k in method3:
                                    for q in j.invokes:
                                        if q.name == k.name:
                                            lists.append(l)
                                            b = True
                                            break
                        if b == False:
                            for j in method21:
                                if i.name == j.name:
                                    for k in method2:
                                        for q in j.invokes:
                                            if q.name == k.name:
                                                lists.append(l)

            final_patterns[x] = lists
        elif x == "COMPOSITE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                if 'SAGG' in l.keys():
                    lists.append(l)
                elif 'IAGG' in l.keys():
                    sp_classes = l.get('IAGG')
                    method1 = classes[sp_classes[0]].operations
                    method2 = classes[sp_classes[1]].operations
                    for i in method1:
                        for j in method2:
                            if i.name == j.name:
                                for q in j.invokes:
                                    if q.name == i.name:
                                        lists.append(l)

                elif 'IIAGG' in l.keys():
                    sp_classes = l.get('IIAGG')
                    method1 = classes[sp_classes[0]].operations
                    method2 = classes[sp_classes[1]].operations
                    for i in method1:
                        for j in method2:
                            if i.name == j.name:
                                for q in j.invokes:
                                    if q.name == i.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "BRIDGE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('IPAG')
                method11 = classes[sp_classes1[0]].operations
                method21 = classes[sp_classes1[1]].operations
                for i in method11:
                    for j in method21:
                        if i.name == j.name:
                            b = False
                            for k in method1:
                                for m in method2:
                                    if k.name == m.name:
                                        for q in j.invokes:
                                            if q.name == k.name:
                                                lists.append(l)
                                                b = True
                                                break
                            if b == False:
                                for k in method1:
                                    for m in method3:
                                        if k.name == m.name:
                                            for q in j.invokes:
                                                if q.name == k.name:
                                                    lists.append(l)

            final_patterns[x] = lists
        elif x == "DECORATOR":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('IAGG')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                sp_classes1 = l.get('CI')
                method11 = classes[sp_classes1[0]].operations
                method21 = classes[sp_classes1[1]].operations
                method31 = classes[sp_classes1[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for q in j.invokes:
                                if q.name == i.name:
                                    lists.append(l)
                        elif i.name == j.name:
                            b = False
                            for m in method21:
                                if i.name == m.name:
                                    for q in m.invokes:
                                        if q.name == i.name:
                                            lists.append(l)
                                            b = True
                                            break
                            if b == False:
                                for m in method31:
                                    if i.name == m.name:
                                        for q in m.invokes:
                                            if q.name == i.name:
                                                lists.append(l)

            final_patterns[x] = lists
        elif x == "FLYWEIGHT":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('AGPI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method3:
                    if i.return_type == classes[sp_classes[0]].name:
                        for j in method2:
                            if j.name == classes[sp_classes[1]].name:
                                for q in i.invokes:
                                    if q.name == j.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "FACADE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICD')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                for q in j.invokes:
                                    if q.name == k.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "ABSTRACT FACTORY":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICD')
                method1 = classes[sp_classes[0]].operations
                sp_classes1 = l.get('DCI')
                method31 = classes[sp_classes1[2]].operations
                sp_classes2 = l.get('CI')
                method22 = classes[sp_classes2[1]].operations
                method32 = classes[sp_classes2[2]].operations
                for i in method1:
                    for j in method31:
                        if i == j:
                            b = True
                            if j.return_type == classes[sp_classes2[1]].name:
                                for m in method22:
                                    if m == classes[sp_classes2[1]].name:
                                        for q in j.invokes:
                                            if q.name == m.name:
                                                lists.append(l)
                                                b = True
                                                break
                            if b == False:
                                if j.return_type == classes[sp_classes2[2]].name:
                                    for m in method32:
                                        if m.name == classes[sp_classes2[2]].name:
                                            for q in j.invokes:
                                                if q.name == m.name:
                                                    lists.append(l)
            final_patterns[x] = lists
        elif x == "BUILDER":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICA')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('AGPI')
                method31 = classes[sp_classes1[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                for q in k.invokes:
                                    if q.name == k.name:
                                        for m in method31:
                                            for z in m.invokes:
                                                if z.name == i.name or z.name == j.name:
                                                    lists.append(l)
            final_patterns[x] = lists
        elif x == "FACTORY METHOD":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICD')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                sp_classes1 = l.get('DCI')
                method11 = classes[sp_classes1[0]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method11:
                                for q in j.invokes:
                                    if q.name == k.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "PROTOTYPE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                if classes[sp_classes[1]].parent == "cloneable":
                    lists.append(l)
                elif classes[sp_classes[2]].parent == "cloneable":
                    lists.append(l)
            final_patterns[x] = lists
        elif x == "SINGLETON":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('SASS')
                methods = classes[sp_classes].operations
                for z in methods:
                    if z.name == classes[sp_classes].name:
                        if z.visibility == "protected":
                            lists.append(l)
                            break
                        elif z.visibility == "private":
                            lists.append(l)
                            break
                        elif z.visibility == "public":
                            if z.static == 1:
                                lists.append(l)
                                break
            final_patterns[x] = lists
        elif x == "CHAIN OF RESPONSIBILITY":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for j in method2:
                        for k in method3:
                            if i.name == j.name and i.name == k.name:
                                b = False
                                for q in j.invokes:
                                    if q.name == i.name:
                                        lists.append(l)
                                        b = True
                                if b == False:
                                    for q in k.invokes:
                                        if q.name == i.name:
                                            lists.append(l)
            final_patterns[x] = lists
        elif x == "COMMAND":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICA')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('AGPI')
                method11 = classes[sp_classes1[0]].operations
                method21 = classes[sp_classes1[1]].operations
                method31 = classes[sp_classes1[2]].operations
                b = False
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                for q in j.invokes:
                                    if q.name == k.name:
                                        b = True
                                        break
                if b == True:
                    for i in method31:
                        for j in method11:
                            for q in j.invokes:
                                if q.name == i.name:
                                    for m in method21:
                                        if j.name == m.name:
                                            lists.append(l)
            final_patterns[x] = lists
        elif x == "INTERPRETER":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                b = False
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('IPD')
                method21 = classes[sp_classes1[1]].operations
                method31 = classes[sp_classes1[2]].operations
                for i in method1:
                    for j in method2:
                        for k in method3:
                            if i.name == j.name and i.name == k.name:
                                b = True
                                break
                if b == True:
                    c = False
                    for j in method2:
                        for i in method21:
                            if j.name != i.name:
                                for m in method31:
                                    for q in j.invokes:
                                        if q.name == m.name:
                                            lists.append(l)
                                            c = True
                                            break
                    if c == False:
                        for j in method3:
                            for i in method21:
                                if j.name != i.name:
                                    for m in method31:
                                        for q in j.invokes:
                                            if q.name == m.name:
                                                lists.append(l)
            final_patterns[x] = lists
        elif x == "ITERATOR":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICD')
                if sp_classes[2].return_type == classes[sp_classes[1]].name:
                    lists.append(l)
                    break
            final_patterns[x] = lists
        elif x == "MEDIATOR":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                b = False
                sp_classes = l.get('IPAS')
                method1 = classes[sp_classes[0]].operations
                sp_classes1 = l.get('ICA')
                method21 = classes[sp_classes1[1]].operations
                method11 = classes[sp_classes1[0]].operations
                for i in method11:
                    for j in method21:
                        if i.name == j.name:
                            b = True
                            break
                if b == True:
                    for i in method1:
                        if i.return_type == classes[sp_classes[2]].name:
                            for j in method21:
                                for q in j.invokes:
                                    if q.name == i.name:
                                        lists.append(l)
            final_patterns[x] = lists
        elif x == "MEMENTO":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                b = False
                sp_classes = l.get('AGPI')
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('DPI')
                method21 = classes[sp_classes1[1]].operations
                method31 = classes[sp_classes1[2]].operations
                for i in method3:
                    if i.return_type == classes[sp_classes[0]].name:
                        b = True
                        break
                if b == True:
                    for i in method21:
                        for j in method31:
                            for q in j.invokes:
                                if q.name == i.name:
                                    lists.append(l)
            final_patterns[x] = lists
        elif x == "OBSERVER":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('AGPI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for m in method3:
                                for q in m.invokes:
                                    if q.name == i.name:
                                        lists.append(l)
            final_patterns[x] = lists
        elif x == "STATE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    b = False
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                if k.name == classes[sp_classes[1]].name:
                                    for q in j.invokes:
                                        if q.name == k.name:
                                            lists.append(l)
                                            b = True
                                            break
                    if b == False:
                        for j in method3:
                            if i.name == j.name:
                                for k in method2:
                                    if k.name == classes[sp_classes[2]].name:
                                        for q in j.invokes:
                                            if q.name == k.name:
                                                lists.append(l)
            final_patterns[x] = lists
        elif x == "STRATEGY":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('AGPI')
                method31 = classes[sp_classes1[2]].operations
                for i in method1:
                    b = False
                    for j in method2:
                        if i.name == j.name:
                            for m in method31:
                                for q in m.invokes:
                                    if q.name == i.name:
                                        lists.append(l)
                                        b = True
                                        break
                    if b == False:
                        for j in method3:
                            if i.name == j.name:
                                for m in method31:
                                    for q in m.invokes:
                                        if q.name == i.name:
                                            lists.append(l)
                                            b = True
            final_patterns[x] = lists
        elif x == "TEMPLATE METHOD":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for q in i.invokes:
                        if q.name == i.name:
                            b = False
                            for j in method2:
                                if i.name == j.name:
                                    lists.append(l)
                                    b = True
                                    break
                            if b == False:
                                for j in method3:
                                    if i.name == j.name:
                                        lists.append(l)
                                        break
            final_patterns[x] = lists
        elif x == "VISITOR":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('AGPI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                sp_classes1 = l.get('ICD')
                method11 = classes[sp_classes1[0]].operations
                method31 = classes[sp_classes1[2]].operations
                b = False
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            b = True
                            break
                if b == True:
                    for i in method31:
                        for j in method11:
                            for q in i.invokes:
                                if q.name == j.name:
                                    lists.append(l)
            final_patterns[x] = lists
    return final_patterns


def method_signatures_full_str(patterns_dict: dict, classes: list):
    final_patterns = {}
    for x in patterns_dict.keys():
        if x == "ADAPTER":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICA')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                for q in j.invokes:
                                    if q == k.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "PROXY":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                if 'ICA' in l.keys():
                    b = False
                    for i in method1:
                        for j in method2:
                            if i.name == j.name:
                                for k in method3:
                                    for q in j.invokes:
                                        if q == k.name:
                                            lists.append(l)
                                            b = True
                                            break
                        if b == False:
                            for j in method3:
                                if i.name == j.name:
                                    for k in method2:
                                        for q in j.invokes:
                                            if q == k.name:
                                                lists.append(l)

                else:
                    sp_classes1 = l.get('CI')
                    method11 = classes[sp_classes1[0]].operations
                    method21 = classes[sp_classes1[1]].operations
                    b = False
                    for i in method11:
                        for j in method21:
                            if i.name == j.name:
                                for k in method3:
                                    for q in j.invokes:
                                        if q == k.name:
                                            lists.append(l)
                                            b = True
                                            break
                        if b == False:
                            for j in method21:
                                if i.name == j.name:
                                    for k in method2:
                                        for q in j.invokes:
                                            if q == k.name:
                                                lists.append(l)

            final_patterns[x] = lists
        elif x == "COMPOSITE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                if 'SAGG' in l.keys():
                    lists.append(l)
                elif 'IAGG' in l.keys():
                    sp_classes = l.get('IAGG')
                    method1 = classes[sp_classes[0]].operations
                    method2 = classes[sp_classes[1]].operations
                    for i in method1:
                        for j in method2:
                            if i.name == j.name:
                                for q in j.invokes:
                                    if q == i.name:
                                        lists.append(l)

                elif 'IIAGG' in l.keys():
                    sp_classes = l.get('IIAGG')
                    method1 = classes[sp_classes[0]].operations
                    method2 = classes[sp_classes[1]].operations
                    for i in method1:
                        for j in method2:
                            if i.name == j.name:
                                for q in j.invokes:
                                    if q == i.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "BRIDGE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('IPAG')
                method11 = classes[sp_classes1[0]].operations
                method21 = classes[sp_classes1[1]].operations
                for i in method11:
                    for j in method21:
                        if i.name == j.name:
                            b = False
                            for k in method1:
                                for m in method2:
                                    if k.name == m.name:
                                        for q in j.invokes:
                                            if q == k.name:
                                                lists.append(l)
                                                b = True
                                                break
                            if b == False:
                                for k in method1:
                                    for m in method3:
                                        if k.name == m.name:
                                            for q in j.invokes:
                                                if q == k.name:
                                                    lists.append(l)

            final_patterns[x] = lists
        elif x == "DECORATOR":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('IAGG')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                sp_classes1 = l.get('CI')
                method11 = classes[sp_classes1[0]].operations
                method21 = classes[sp_classes1[1]].operations
                method31 = classes[sp_classes1[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for q in j.invokes:
                                if q == i.name:
                                    lists.append(l)
                        elif i.name == j.name:
                            b = False
                            for m in method21:
                                if i.name == m.name:
                                    for q in m.invokes:
                                        if q == i.name:
                                            lists.append(l)
                                            b = True
                                            break
                            if b == False:
                                for m in method31:
                                    if i.name == m.name:
                                        for q in m.invokes:
                                            if q == i.name:
                                                lists.append(l)

            final_patterns[x] = lists
        elif x == "FLYWEIGHT":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('AGPI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method3:
                    if i.return_type == classes[sp_classes[0]].name:
                        for j in method2:
                            if j.name == classes[sp_classes[1]].name:
                                for q in i.invokes:
                                    if q == j.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "FACADE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICD')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                for q in j.invokes:
                                    if q == k.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "ABSTRACT FACTORY":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICD')
                method1 = classes[sp_classes[0]].operations
                sp_classes1 = l.get('DCI')
                method31 = classes[sp_classes1[2]].operations
                sp_classes2 = l.get('CI')
                method22 = classes[sp_classes2[1]].operations
                method32 = classes[sp_classes2[2]].operations
                for i in method1:
                    for j in method31:
                        if i == j:
                            b = True
                            if j.return_type == classes[sp_classes2[1]].name:
                                for m in method22:
                                    if m == classes[sp_classes2[1]].name:
                                        for q in j.invokes:
                                            if q == m.name:
                                                lists.append(l)
                                                b = True
                                                break
                            if b == False:
                                if j.return_type == classes[sp_classes2[2]].name:
                                    for m in method32:
                                        if m.name == classes[sp_classes2[2]].name:
                                            for q in j.invokes:
                                                if q == m.name:
                                                    lists.append(l)
            final_patterns[x] = lists
        elif x == "BUILDER":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICA')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('AGPI')
                method31 = classes[sp_classes1[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                for q in k.invokes:
                                    if q == k.name:
                                        for m in method31:
                                            for z in m.invokes:
                                                if z == i.name or z == j.name:
                                                    lists.append(l)
            final_patterns[x] = lists
        elif x == "FACTORY METHOD":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICD')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                sp_classes1 = l.get('DCI')
                method11 = classes[sp_classes1[0]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method11:
                                for q in j.invokes:
                                    if q == k.name:
                                        lists.append(l)

            final_patterns[x] = lists
        elif x == "PROTOTYPE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                if classes[sp_classes[1]].parent == "cloneable":
                    lists.append(l)
                elif classes[sp_classes[2]].parent == "cloneable":
                    lists.append(l)
            final_patterns[x] = lists
        elif x == "SINGLETON":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('SASS')
                methods = classes[sp_classes].operations
                for z in methods:
                    if z.name == classes[sp_classes].name:
                        if z.visibility == "protected":
                            lists.append(l)
                            break
                        elif z.visibility == "private":
                            lists.append(l)
                            break
                        elif z.visibility == "public":
                            if z.static == 1:
                                lists.append(l)
                                break
            final_patterns[x] = lists
        elif x == "CHAIN OF RESPONSIBILITY":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for j in method2:
                        for k in method3:
                            if i.name == j.name and i.name == k.name:
                                b = False
                                for q in j.invokes:
                                    if q == i.name:
                                        lists.append(l)
                                        b = True
                                if b == False:
                                    for q in k.invokes:
                                        if q == i.name:
                                            lists.append(l)
            final_patterns[x] = lists
        elif x == "COMMAND":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICA')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('AGPI')
                method11 = classes[sp_classes1[0]].operations
                method21 = classes[sp_classes1[1]].operations
                method31 = classes[sp_classes1[2]].operations
                b = False
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                for q in j.invokes:
                                    if q == k.name:
                                        b = True
                                        break
                if b == True:
                    for i in method31:
                        for j in method11:
                            for q in j.invokes:
                                if q == i.name:
                                    for m in method21:
                                        if j.name == m.name:
                                            lists.append(l)
            final_patterns[x] = lists
        elif x == "INTERPRETER":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                b = False
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('IPD')
                method21 = classes[sp_classes1[1]].operations
                method31 = classes[sp_classes1[2]].operations
                for i in method1:
                    for j in method2:
                        for k in method3:
                            if i.name == j.name and i.name == k.name:
                                b = True
                                break
                if b == True:
                    c = False
                    for j in method2:
                        for i in method21:
                            if j.name != i.name:
                                for m in method31:
                                    for q in j.invokes:
                                        if q == m.name:
                                            lists.append(l)
                                            c = True
                                            break
                    if c == False:
                        for j in method3:
                            for i in method21:
                                if j.name != i.name:
                                    for m in method31:
                                        for q in j.invokes:
                                            if q == m.name:
                                                lists.append(l)
            final_patterns[x] = lists
        elif x == "ITERATOR":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('ICD')
                if sp_classes[2].return_type == classes[sp_classes[1]].name:
                    lists.append(l)
                    break
            final_patterns[x] = lists
        elif x == "MEDIATOR":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                b = False
                sp_classes = l.get('IPAS')
                method1 = classes[sp_classes[0]].operations
                sp_classes1 = l.get('ICA')
                method21 = classes[sp_classes1[1]].operations
                method11 = classes[sp_classes1[0]].operations
                for i in method11:
                    for j in method21:
                        if i.name == j.name:
                            b = True
                            break
                if b == True:
                    for i in method1:
                        if i.return_type == classes[sp_classes[2]].name:
                            for j in method21:
                                for q in j.invokes:
                                    if q == i.name:
                                        lists.append(l)
            final_patterns[x] = lists
        elif x == "MEMENTO":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                b = False
                sp_classes = l.get('AGPI')
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('DPI')
                method21 = classes[sp_classes1[1]].operations
                method31 = classes[sp_classes1[2]].operations
                for i in method3:
                    if i.return_type == classes[sp_classes[0]].name:
                        b = True
                        break
                if b == True:
                    for i in method21:
                        for j in method31:
                            for q in j.invokes:
                                if q == i.name:
                                    lists.append(l)
            final_patterns[x] = lists
        elif x == "OBSERVER":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('AGPI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            for m in method3:
                                for q in m.invokes:
                                    if q == i.name:
                                        lists.append(l)
            final_patterns[x] = lists
        elif x == "STATE":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    b = False
                    for j in method2:
                        if i.name == j.name:
                            for k in method3:
                                if k.name == classes[sp_classes[1]].name:
                                    for q in j.invokes:
                                        if q == k.name:
                                            lists.append(l)
                                            b = True
                                            break
                    if b == False:
                        for j in method3:
                            if i.name == j.name:
                                for k in method2:
                                    if k.name == classes[sp_classes[2]].name:
                                        for q in j.invokes:
                                            if q == k.name:
                                                lists.append(l)
            final_patterns[x] = lists
        elif x == "STRATEGY":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                sp_classes1 = l.get('AGPI')
                method31 = classes[sp_classes1[2]].operations
                for i in method1:
                    b = False
                    for j in method2:
                        if i.name == j.name:
                            for m in method31:
                                for q in m.invokes:
                                    if q == i.name:
                                        lists.append(l)
                                        b = True
                                        break
                    if b == False:
                        for j in method3:
                            if i.name == j.name:
                                for m in method31:
                                    for q in m.invokes:
                                        if q == i.name:
                                            lists.append(l)
                                            b = True
            final_patterns[x] = lists
        elif x == "TEMPLATE METHOD":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('CI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                method3 = classes[sp_classes[2]].operations
                for i in method1:
                    for q in i.invokes:
                        if q == i.name:
                            b = False
                            for j in method2:
                                if i.name == j.name:
                                    lists.append(l)
                                    b = True
                                    break
                            if b == False:
                                for j in method3:
                                    if i.name == j.name:
                                        lists.append(l)
                                        break
            final_patterns[x] = lists
        elif x == "VISITOR":
            lists = []
            class_list = patterns_dict.get(x)
            for l in class_list:
                sp_classes = l.get('AGPI')
                method1 = classes[sp_classes[0]].operations
                method2 = classes[sp_classes[1]].operations
                sp_classes1 = l.get('ICD')
                method11 = classes[sp_classes1[0]].operations
                method31 = classes[sp_classes1[2]].operations
                b = False
                for i in method1:
                    for j in method2:
                        if i.name == j.name:
                            b = True
                            break
                if b == True:
                    for i in method31:
                        for j in method11:
                            for q in i.invokes:
                                if q == j.name:
                                    lists.append(l)
            final_patterns[x] = lists
    return final_patterns