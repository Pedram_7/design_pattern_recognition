import numpy as np

subpattern_matrices = []

subpattern_matrices.append(('ICA', np.array([[1, 1, 1],
                                             [3, 1, 2],
                                             [1, 1, 1]])))
subpattern_matrices.append(('CI', np.array([[1, 1, 1],
                                            [3, 1, 1],
                                            [3, 1, 1]])))

subpattern_matrices.append(('IAGG', np.array([[1, 1],
                                              [3 * 5, 1]])))

subpattern_matrices.append(('IPAG', np.array([[1, 1, 5],
                                              [3, 1, 1],
                                              [1, 1, 1]])))

subpattern_matrices.append(('MLI', np.array([[1, 1, 1],
                                             [3, 1, 1],
                                             [1, 3, 1]])))

subpattern_matrices.append(('IASS', np.array([[1, 1],
                                              [3 * 2, 1]])))

subpattern_matrices.append(('SAGG', np.array([[5]])))

subpattern_matrices.append(('IIAGG', np.array([[1, 1, 1],
                                               [3, 1, 1],
                                               [5, 3, 1]])))

subpattern_matrices.append(('SASS', np.array([[2]])))

subpattern_matrices.append(('ICD', np.array([[1, 1, 1],
                                             [3, 1, 7],
                                             [1, 1, 1]])))

subpattern_matrices.append(('DCI', np.array([[1, 1, 1],
                                             [3, 1, 1],
                                             [1, 7, 1]])))

subpattern_matrices.append(('IPAS', np.array([[1, 1, 2],
                                              [3, 1, 1],
                                              [1, 1, 1]])))

subpattern_matrices.append(('AGPI', np.array([[1, 1, 1],
                                              [3, 1, 1],
                                              [5, 1, 1]])))

subpattern_matrices.append(('IPD', np.array([[1, 1, 7],
                                             [3, 1, 1],
                                             [1, 1, 1]])))

subpattern_matrices.append(('DPI', np.array([[1, 1, 1],
                                             [3, 1, 1],
                                             [7, 1, 1]])))

print('{} subpattern matrices initiated.'.format(len(subpattern_matrices)))

# PATTERNS

patterns = {
    "ADAPTER": [(['ICA', '!CI'], [[0, {1: 1, 2: 2, 3: 3}],
                                  [0, 0]]),
                (['ICA', '!CI'], [[0, {1: 1, 2: 3, 3: 2}],
                                  [0, 0]])],  # NOT CI !

    "PROXY": [(['CI', 'ICA'], [[0, {1: 1, 2: 2, 3: 3}], [0, 0]]), (['CI', 'ICA'], [[0, {1: 1, 2: 3, 3: 2}], [0, 0]]),
              (['CI', 'IASS'], [[0, {1: 1, 2: 2}], [0, 0]]), (['CI', 'IASS'], [[0, {1: 1, 3: 2}], [0, 0]])],

    "COMPOSITE": [(['SAGG'], [{1: 1}]),
                  (['CI', 'IAGG'], [[0, {1: 1, 2: 2}],
                                    [0, 0]]),
                  (['CI', 'IAGG'], [[0, {1: 1, 3: 2}],
                                    [0, 0]]),
                  ((['CI', 'IIAGG'], [[0, {1: 1, 2: 2}],
                                      [0, 0]])),
                  (['CI', 'IIAGG'], [[0, {1: 1, 3: 2}],
                                     [0, 0]])],

    "BRIDGE": [(['CI', 'IPAG'], [[0, {1: 3}],
                                 [0, 0]])],

    "DECORATOR": [(['CI', 'IAGG', 'MLI'], [[0, {1: 1, 2: 2}, {1: 1, 2: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]]),
                  (['CI', 'IAGG', 'MLI'], [[0, {1: 1, 3: 2}, {1: 1, 2: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]]),
                  (['CI', 'IAGG', 'MLI'], [[0, {1: 1, 2: 2}, {1: 1, 3: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]]),
                  (['CI', 'IAGG', 'MLI'], [[0, {1: 1, 3: 2}, {1: 1, 3: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]])],
    "FLYWEIGHT": [(['CI', 'AGPI'], [[0, {1: 1, 2: 2}], [0, 0]]), (['CI', 'AGPI'], [[0, {1: 1, 3: 2}], [0, 0]])],
    # "FACADE": [(['ICD', 'ICD', 'ICD'], [[0, {1: 1, 2: 2}, {1: 1, 2: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]])],
    "ABSTRACT FACTORY": [(['CI', 'DCI', 'ICD'], [[0, {1: 1, 2: 2}, {2: 3}], [0, 0, {3: 2, 2: 3}], [0, 0, 0]]),
                         (['CI', 'DCI', 'ICD'], [[0, {1: 1, 3: 2}, {2: 3}], [0, 0, {3: 2, 2: 3}], [0, 0, 0]]),
                         (['CI', 'DCI', 'ICD'], [[0, {1: 1, 2: 2}, {3: 3}], [0, 0, {3: 2, 2: 3}], [0, 0, 0]]),
                         (['CI', 'DCI', 'ICD'], [[0, {1: 1, 3: 2}, {3: 3}], [0, 0, {3: 2, 2: 3}], [0, 0, 0]])],
    "BUILDER": [(['ICA', 'AGPI'], [[0, {1: 1, 2: 2}], [0, 0]])],
    "FACTORY METHOD": [(['ICD', 'DCI'], [[0, {2: 3, 3: 3}], [0, 0]])],
    "PROTOTYPE": [(['CI', 'AGPI'], [[0, {1: 1, 2: 2}], [0, 0]]), (['CI', 'AGPI'], [[0, {1: 1, 3: 2}], [0, 0]])],
    "SINGLETON": [(['SASS'], [0])],
    "CHAIN OF RESPONSIBILITY": [(['SASS', 'CI'], [0, {1: 1}, [0, 0]])],
    "COMMAND": [(['ICA', 'AGPI'], [[0, {1: 1, 2: 2}], [0, 0]])],
    "INTERPRETER": [(['CI', 'IAGG', 'IPD'], [[0, {1: 1, 2: 2}, {1: 1, 2: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]]),
                    (['CI', 'IAGG', 'IPD'], [[0, {1: 1, 3: 2}, {1: 1, 2: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]]),
                    (['CI', 'IAGG', 'IPD'], [[0, {1: 1, 2: 2}, {1: 1, 3: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]]),
                    (['CI', 'IAGG', 'IPD'], [[0, {1: 1, 3: 2}, {1: 1, 3: 2}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]])],
    "ITERATOR": [(['ICA', 'ICD', 'DCI'], [[0, {2: 3, 3: 2}, {1: 1, 2: 2, 3: 3}], [0, 0, {2: 3, 3: 2}], [0, 0, 0]])],
    "MEDIATOR": [(['CI', 'IPAS', 'ICA'], [[0, {1: 1, 2: 2}, {2: 3}], [0, 0, {3: 1}], [0, 0, 0]]),
                 (['CI', 'IPAS', 'ICA'], [[0, {1: 1, 3: 2}, {2: 3}], [0, 0, {3: 1}], [0, 0, 0]]),
                 (['CI', 'IPAS', 'ICA'], [[0, {1: 1, 2: 2}, {3: 3}], [0, 0, {3: 1}], [0, 0, 0]]),
                 (['CI', 'IPAS', 'ICA'], [[0, {1: 1, 3: 2}, {3: 3}], [0, 0, {3: 1}], [0, 0, 0]])],
    "MEMENTO": [(['DPI', 'AGPI'], [[0, {1: 1, 2: 2}], [0, 0]])],
    "OBSERVER": [(['AGPI', 'ICD'], [[0, {1: 1, 2: 2}], [0, 0]])],
    "STATE": [(['CI', 'AGPI'], [[0, {1: 1, 2: 2}], [0, 0]]), (['CI', 'AGPI'], [[0, {1: 1, 3: 2}], [0, 0]])],
    "STRATEGY": [(['CI', 'AGPI'], [[0, {1: 1, 2: 2}], [0, 0]]), (['CI', 'AGPI'], [[0, {1: 1, 3: 2}], [0, 0]])],
    "TEMPLATE METHOD": [(['CI'], [0])],
    "VISITOR": [(['AGPI', 'DPI', 'ICD'], [[0, {1: 3}, {2: 3}], [0, 0, {1: 1, 2: 2}], [0, 0, 0]])]
}

system_classes = []
