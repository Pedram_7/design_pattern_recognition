import xml
from xml.dom.minidom import parse

import graph_matching
import constants
from parsers import getConnectionsList, getClassesList, create_matrix
from export import export_patterns_partial, export_subpatterns

if __name__ == "__main__":
    doc = xml.dom.minidom.parse("JHotDraw-XMl1.1.xml")
    connection_names = ["Association", "Generalization", "Dependency"]
    connections = getConnectionsList(doc, connection_names)
    constants.system_classes = getClassesList(doc)

    system_matrix = create_matrix(classes=constants.system_classes, connections=connections)
    all_subpattern_instance_sets = graph_matching.calculate_all_subpatterns(constants.subpattern_matrices,
                                                                            system_matrix)
    # export_subpatterns(all_subpattern_instance_sets, 'Junit-subpatterns2.xlsx')

    # dict name->list of tuples
    all_pattern_instance_sets_raw = graph_matching.calculate_all_patterns_partial(constants.patterns,
                                                                                  all_subpattern_instance_sets)
    export_patterns_partial(all_pattern_instance_sets_raw, 'JHotDraw-patterns-partial.xlsx')
    # dict name -> list of dicts
    # all_pattern_instance_sets_method_checked = graph_matching.detect_pattern(patterns, all_subpattern_instance_sets)
    # dict name -> list of dicts
